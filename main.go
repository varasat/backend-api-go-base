package main

import (
	"backend-api-go-base/handlers"
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"net/http"
)

func main() {
	//Homework:
	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN: "host=db user=myuser password=mypassword dbname=customer-db port=5432 sslmode=disable",
	}), &gorm.Config{})
	if err != nil {
		log.Fatal("Failed to connect to database:", err)
	}

	fmt.Println("connected to db")
	customerAddEndpoint := handlers.NewCustomerAddHandler(db)
	customerGetEndpoint := handlers.NewCustomerGetHandler(db)

	http.HandleFunc("/customer", handlers.HealthHandler)
	http.HandleFunc("/customer/add", customerAddEndpoint.ServeHTTP)
	http.HandleFunc("/customer/get", customerGetEndpoint.ServeHTTP)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
