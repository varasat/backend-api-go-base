package models

type Customer struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type CustomerDB struct {
	Name       string `gorm:"column:name"`
	CustomerId string `gorm:"column:id"`
}

func (c *CustomerDB) TableName() string {
	return "customers" // Specify the table name explicitly
}

func (c *Customer) fromModel() CustomerDB {
	return CustomerDB{
		Name: c.Name,
	}
}
