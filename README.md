# Backend-api-go-base

Hello! I'm Andy, and I've been a software engineer since 2015. 
I have ADHD and thusly I find it hard to *start* anything. 

This repo is done entirely for people like me that need a basic starting point. You may fork it, write your own improvements (I've added some Homework comments in some places that might be helpful).  

Don't let that inner saboteur get you. If your interviewer says "Oh yeah the test at home will only be like a 3 hour project", use this project to actually make it 3 hours long. 

For any suggestions just ping me a message at varasatt@gmail.com

## Getting started

- [ ] To run the project just make sure you have docker installed and at least version 1.18 of Go
- [ ] To run the containerized API you should be able to just run the following command
```
make run
```


## Adding a new endpoint

- [ ] Add a new file in the `handlers` folder, follow the example of the already existent endpoints


## Roadmap
- [X] Add POST/GET examples
- [X] Containerize with Docker
- [X] Create a DB
- [X] Integrate DB logic in the endpoint handlers
- [ ] Add tests to the current endpoints and any related functions
- [ ] Add examples of PUT/PATCH/DELETE operations

## Contributions
Feel free to Fork from this repo and create your own Go REST APIs

## Author
Andrei Manolache
## License
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Project status
Active