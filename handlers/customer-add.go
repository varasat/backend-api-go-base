package handlers

import (
	"backend-api-go-base/models"
	"encoding/json"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"net/http"
)

// CustomerAddHandler Dependency injector
type CustomerAddHandler struct {
	DB *gorm.DB
}

func NewCustomerAddHandler(db *gorm.DB) *CustomerAddHandler {
	return &CustomerAddHandler{DB: db}
}

func (c *CustomerAddHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Check if the appropriate method is used
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}

	// Read the request body
	var customer models.Customer
	err := json.NewDecoder(r.Body).Decode(&customer)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad request"))
		return
	}

	// Convert Customer to CustomerDB
	customerDB := models.CustomerDB{
		CustomerId: uuid.New().String(),
		Name:       customer.Name,
	}

	// Insert the new customer into the database
	result := c.DB.Create(customerDB)
	if result.Error != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal server error"))
		return
	}

	// Return response
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Success"))
}
