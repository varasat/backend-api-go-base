package handlers

import (
	"backend-api-go-base/handlers/helpers"
	handlers "backend-api-go-base/handlers/interfaces"
	"backend-api-go-base/models"
	"gorm.io/gorm"
	"net/http"
)

type CustomerGetHandler struct {
	DB *gorm.DB
}

func NewCustomerGetHandler(db *gorm.DB) *CustomerGetHandler {
	return &CustomerGetHandler{DB: db}
}

func (c *CustomerGetHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}
	customerId := r.URL.Query().Get("customerID")
	if customerId == "" {
		w.WriteHeader(http.StatusNotAcceptable)
		w.Write([]byte("Customer ID is empty"))
		return
	}

	result := models.CustomerDB{}
	c.DB.Where("id = ?", customerId).First(&result)
	if result.CustomerId == "" {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Customer not found"))
		return
	}

	res := handlers.CustomerResponse{
		Customer: &models.Customer{
			ID:   result.CustomerId,
			Name: result.Name,
		},
	}

	helpers.WriteResponse(w, res, http.StatusOK)
	return
}
