package handlers

import (
	"backend-api-go-base/models"
	"net/http"
)

type CustomerHandlerInterface interface {
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}

type CustomerResponse struct {
	Customer *models.Customer `json:"customer"`
}
